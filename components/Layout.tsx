import React, { ReactNode } from 'react'
import Link from 'next/link'
import Head from 'next/head'
import { Box, Grid, Grommet, Text, Footer, Anchor } from 'grommet';

type Props = {
    children?: ReactNode
    title?: string
}

const Layout = ({ children, title = 'This is the default title' }: Props) => (
    <Grommet plain>
        <Head>
            <title>{title}</title>
            <meta charSet="utf-8" />
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
        <footer>
            <Grid
                columns={["1/4", "3/4"]}
                rows={['xxsmall', 'large', 'small']}
                gap="small"
                alignSelf="center"
                areas={[
                    { name: 'header', start: [0, 0], end: [1, 0] },
                    { name: 'nav', start: [0, 1], end: [0, 1] },
                    { name: 'main', start: [1, 1], end: [1, 1] },
                    { name: 'footer', start: [0, 2], end: [1, 2] },
                ]}>
                <Box gridArea="header" background="brand">
                    <header>
                        <nav>
                            <Link href="/">
                              <Anchor label="Home" />
                            </Link>{' '}
                            <Link href="/dashboard">
                              <Anchor label="Dashboard" />
                            </Link>{' '}
                            <Link href="/about">
                              <Anchor label="About" />
                            </Link>{' '}
                            <Link href="/users">
                              <Anchor label="Users" />
                            </Link>{' '}
        | <a href="/api/users">Users API</a>
                        </nav>
                    </header>
                </Box>
                <Box gridArea="nav" background="light-5"></Box>
                <Box gridArea="main" background="light-2">{children}</Box>
                <Box gridArea="footer" background="dark-5">
                    <Footer pad="medium">
                        <Text>Copyright</Text>
                        <Link href="/">
                            <Anchor as="span" label="Home" color="brand" /></Link>
                    </Footer>
                </Box>

            </Grid>
            <hr />
        </footer>
    </Grommet>
)

export default Layout
